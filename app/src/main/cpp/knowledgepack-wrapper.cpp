#include <jni.h>
#include <string>
#include <cinttypes>
#include <android/log.h>
#include <kb.h>
#include <testdata.h>

#define LOGI(...) \
  ((void)__android_log_print(ANDROID_LOG_INFO, "knowledgepack-wrapper::", __VA_ARGS__))
extern "C"
JNIEXPORT void JNICALL
Java_com_sensiml_knowledgepackrunner_MainActivity_initSensiMLKnowledgePackModels(JNIEnv *env,
                                                                                 jclass clazz) {
    kb_model_init();
    LOGI("KB MODEL INIT");
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_sensiml_knowledgepackrunner_MainActivity_runSensiMLKnowledgePackModel(JNIEnv *env,
                                                                               jobject thiz,
                                                                               jint model,
                                                                               jshortArray data,
                                                                               jint data_length) {
#if defined(USE_TEST_RAW_SAMPLES)
    static int last_index;
    int ret = 0;
    int index;
    int num_sensors = 0;
    SENSOR_DATA_T *pData;

    if(last_index >= TD_NUMROWS){
        last_index = 0;
    }

    for (index = last_index; index < TD_NUMROWS; index++) {
        pData = (SENSOR_DATA_T *) &testdata[index];
        ret = kb_run_model((SENSOR_DATA_T *) pData, num_sensors, KB_MODEL_parent_model_INDEX);
        if (ret >= 0) {
            kb_print_model_result(KB_MODEL_parent_model_INDEX, ret);

            kb_reset_model(0);
            last_index = index;
            return ret;
        }
    }
    return ret;

#else
    return -1;
#endif //USE_TEST_RAW_SAMPLES
}